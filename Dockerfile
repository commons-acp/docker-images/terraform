FROM alpine/terragrunt:1.2.6
RUN apk add --no-cache git \
                       maven \
                       curl \
                       python3 \
                       py-pip \
                       npm \
                       wget \
                       tar \
                       zip
RUN /usr/bin/python3 -m pip install requests
RUN /usr/bin/python3 -m pip install python-gitlab
RUN /usr/bin/python3 -m pip install boto3
RUN /usr/bin/python3 -m pip install awscli
RUN apk add --update mysql mysql-client
RUN apk add sshpass


